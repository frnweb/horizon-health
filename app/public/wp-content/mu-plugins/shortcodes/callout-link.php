<?php
	// CALLOUT LINK
		function sl_callout_link ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'url'	=> '#',
				'title'	=> '', 
				'target'	=> '_self'
				), $atts );
				$content = wpautop(trim($content));
				$calloutLink = '<a data-equalizer-watch class="sl_callout-link" href="' . esc_attr($specs['url'] ) . '" target="'. esc_attr($specs['target'] ) .'"><h3>' . esc_attr($specs['title'] ) . '</h3>' . do_shortcode( $content ) . '</a>';

				return '[shortcode_unautop]' . $calloutLink .'[/shortcode_unautop]';
		}

		add_shortcode ('callout-link', 'sl_callout_link' );
	///CALLOUT LINK

?>