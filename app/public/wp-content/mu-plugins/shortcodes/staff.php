<?php
// STAFF CARD Dependent on ZURB Foundation 6
	function sl_staff_card( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'name'		=> '',
			'title'		=> '',
			'preview'		=> '',
			'linkedin'	=> ''
		), $atts );
		static $i = 0;
		$i++;
		if(esc_attr($specs['linkedin'] ) != '') {
			$linkedin_content = '<a class="sl_staff-card__social" target="_blank" href="' . esc_attr($specs['linkedin'] ) . '"><i class="fab fa-linkedin"></i></a>';
		} else {
			$linkedin_content = NULL;
		};

		$content = wpautop(trim($content));
		return '<div class="sl_staff-card"><h3 class="sl_staff-card__name">' . esc_attr($specs['name'] ) . '</h3><p class="sl_staff-card__title">' . esc_attr($specs['title'] ) . '</p><p class="sl_staff-card__preview">' . esc_attr($specs['preview'] ) . '</p>' . $linkedin_content . '<div id="sl_bio' . $i . '" class="sl_staff-card__bio" data-toggler="expanded">' . do_shortcode ( $content ) . '</div><div class="sl_staff-card__button" data-toggle="sl_bio' . $i . ' sl_read' . $i . '"><span id="sl_read' . $i . '" data-toggler="expanded"> Read <span class="sl_read__more">More</span><span class="sl_read__less">Less</span></span></div></div>';
	}
	add_shortcode ('staff', 'sl_staff_card' );
///STAFF CARD
?>