<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],
];

$testimonial = new FieldsBuilder('testimonial');

$testimonial
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$testimonial

	->addTab('content', ['placement' => 'left'])

		//Header		
		->addTrueFalse('header_check', [
			'wrapper' => ['width' => 15],
			'label' => 'Add Header?'
			])
		->addText('header', [
			'label' => 'Pre-header',
			'wrapper' => ['width' => 85]
		])
			->setInstructions('Optional section header above testimonials' )
		->conditional('header_check', '==', 1)

		//Button
		->addGroup('buttons', [
			'label' => 'Optional Module Buttons'
		])
			->addFields(get_field_partial('modules.button'))
		->endGroup()

    	//Repeater
		->addRepeater('testimonial', [
		  'min' => 1,
		  'max' => 6,
		  'button_label' => 'Add Testimonial',
		  'layout' => 'block',
		  'wrapper' => [
	          'class' => 'deck',
	        ],
		])

		//Image 
		->addImage('image')

		// Quote
		->addWysiwyg('quote', [
			'label' => 'Quote',
			'ui' => $config->ui
		])

		// Cite
		->addText('cite', [
			'label' => 'Cite',
			'ui' => $config->ui
		]); 	

return $testimonial;