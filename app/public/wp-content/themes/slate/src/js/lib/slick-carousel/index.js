jQuery(function($) {
  function initSlider(){
    //TESTIMONIAL MODULE
    $(".sl_testimonial__slider").slick({

      // normal options...
      arrows: false,
      infinite: true,
      dots: true,
      slidesToShow: 1,
      appendDots: '.sl_testimonial__controls',
      adaptiveHeight: true,
    });

    //CAROUSEL MODULE
    const carouselSlider = [];

    const carouselDuoSlider = [];

    const CarouselImageSlider = [];

    var carouselExists = document.getElementsByClassName('sl_carousel');
    if (carouselExists.length > 0) {
    // if carousel module exists on page

    myCarouselData.forEach(function(item){
      if (item.type == 'card') {
        carouselSlider.push(item);
      } else if (item.type == 'duo'){
        carouselDuoSlider.push(item);
      } else {
        carouselImageSlider.push(item);
      }
    })

    //Carousel Card Type

      carouselSlider.forEach(function(item){
        $( '.sl_slider--' + item.loopIndex ).slick({

          // normal options...
          prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
          nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
          infinite: true,
          slidesToShow: item.slidesLarge,
        
          // the magic
          responsive: [{
        
              breakpoint: 1024,
              settings: {
                slidesToShow: item.slidesMedium,
                infinite: true
              }
        
            }, {
        
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                infinite: true
              }
        
            }]
      });
      })


      //Carousel Duo Type
      carouselDuoSlider.forEach(function(item){
        $( '.sl_slider-duo--' + item.loopIndex ).slick({

            // normal options...
            prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
            nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
            infinite: true,
            slidesToShow: 1
        });
      });
    };
  };
  jQuery(document).ready(function() {
    initSlider();
  });
});