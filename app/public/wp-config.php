<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dNShAvIFw4a8jvMbqWcFDglfYR82zHtno5AD7az+saQyktitj/jT0AauaBnULFgoDqtoSb1wrHBYs8yvp8lbgA==');
define('SECURE_AUTH_KEY',  'Kcn3zwzzKe61kKymDfl68l2DkFy9Tl4vDAjTSKVr7a3dMYn+qr1ul4S9QsRrIUFfLfRcSlK7FslMfqa9irCepA==');
define('LOGGED_IN_KEY',    'QF0srLZTY5rlKkJvjJOX6/rBQ1HJ+KeA+IwchKS+T+nQeuDd77XySPYa7pr9lK7k3lB7kwZiNjk7RChQAiUPog==');
define('NONCE_KEY',        'ac2zajuY0yff42cMau1goRAJwR8iGo8UuV9BWBTKHN7NyaTro9thdM9QoIqFKp5HskB1Y8UT2NVPB/NkFCKEow==');
define('AUTH_SALT',        'ODKJA2htEWOrDLvrFm1aoDcRNF9UnBMLuosxaTLgY6v97HdzCmIEmMm9yPU3um/dEGW+Tb4Gd3i8IxTbiIM8Sg==');
define('SECURE_AUTH_SALT', 'gumy2d0HYvPs9dITPvlSZLleI8j5nPGD4Wnkmmw722ElAChA4ZJZu7YB1ltJGKJ1QIwvhH2qx4S5kFI4mUlDZw==');
define('LOGGED_IN_SALT',   'IJFmafBkff2pQwjYSxPLbynNH4m653zbNNQeLjcz6j5eG8ZRydp1zezNEDDi8APb/8J4CcDnWEg0J+gu/81+PQ==');
define('NONCE_SALT',       '7RCTglBI6BRmnLtRjCu2QsgL4JHrJBV5Kk95qoSoP4JubXdQUXtT7IXtXTOXXdWjJXIvNn6THH0bsmY3L/qnyA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

// This enables debugging.
define( 'WP_DEBUG', true );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
